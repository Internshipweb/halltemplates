<?php

// sentence.php

return [
  'home'=>'Home',
  'about'=>'About us',
  'contact'=>'Contact',
  'language'=>'Language',
  'account'=>'Account',
   'english'=>'English',
  'myanmar'=>'Myanmar',
  'aboutus'=>'About Us',
  'content'=>'The user can search halls on a single place.Hall owner 
						 can advertise their halls , user can contact hall owner to rent halls and
						 can know locations of halls.',
	'login'=>'Login',
	'logout'=>'Logout',
	'viewer'=>'Viewers'
];
