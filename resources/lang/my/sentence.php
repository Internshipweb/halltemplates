<?php

// sentence.php

return [
	'home' => 'မျက်နှာစာ',
  'about' => 'အကြောင်းအရာ',
  'contact'=>'ဆက်သွယ်ရန်',
  'language'=>'ဘာသာစကား',
  'account'=>'အကောင့်',
  'english'=>'အဂၤလိပ္',
  'myanmar' =>'ျမန္မာ',
  'aboutus'=>'၀က္(ဘ္)ဆိုက္အေျကာင္းအရာ',
  'content'=>'ခန္းမမ်ားကိုအလၤယ္တကူဂွာနိုင္ပာတယ္',
  'login'=>'အေကာင္ံ၀င္ရန္',
  'logout'=>'အေကာင္ံမွထြက္ရန္',
  'viewer'=>'ျကည့္ရုွသူ'
];